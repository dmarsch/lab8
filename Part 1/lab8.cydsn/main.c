/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
uint8 cnt = 0;
uint8 dir_cnt = 0;
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    CLK_Start();
   CLK_SetDividerValue(20);             //set the speed of the motor
    Motor_CTL_Write(0x03);              //enable 1 and direction 1 for cw
    for(;;)
    {
        /* Place your application code here. */
       if (Motor_State_Read() == 0x0A) 
        {
            cnt++;
            while(Motor_State_Read() == 0x0A); //poll this state to verify that the 0x0A state actually occurred
        }
       if (cnt == 10) 
        {      
            //Motor_CTL_Write( Motor_CTL_Read() ^ 0x2); //fancy way to do the direction checking
            if (Motor_CTL_Read() == 0x1) //changes direction by checking which direction it's currently going
            {
                Motor_CTL_Write(0x3);               //on and cw direction
            }
            else {
                Motor_CTL_Write(0x1);               //on and ccw direction
            }
            cnt = 0;                    //set to 0 to continue to allow for 5 direction changes
            dir_cnt++;
        }
        if (dir_cnt == 5)
        {
            Motor_CTL_Write(0x00);      //motor turns off
            CyDelay(2000);                 //turns off for 2 seconds
            Motor_CTL_Write(0x3);
            cnt = 0;
            dir_cnt = 0;                //reset counters to start loop again
        }
    }
}
/* [] END OF FILE */

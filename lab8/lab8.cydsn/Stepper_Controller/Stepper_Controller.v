
//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line
// Generated on 10/22/2019 at 19:22
// Component: Stepper_Controller
module Stepper_Controller (
	output  Black,
	output  Brown,
	output  Orange,
	output  Yellow,
	input   clk,
	input   Direction,
	input   Enable
);

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here
reg [3:0] count;
assign Black = count[0];
assign Brown = count[1];
assign Orange = count[2];
assign Yellow = count[3];

always @(posedge clk) begin
    if (Enable) begin
        if (Direction) begin
            case(count) //cw sequence
            4'b1010 : count <= 4'b1001;
            4'b1001 : count <= 4'b0101;
            4'b0101 : count <= 4'b0110;
            4'b0110 : count <= 4'b1010;
            default : count <= 4'b1010;
            endcase
        end else begin
            case(count) //ccw sequence
            4'b1010 : count <= 6;
            4'b1001 : count <= 10;
            4'b0101 : count <= 9;
            4'b0110 : count <= 5;
            default : count <= 10;
            endcase
        end
    end
    else begin
        count <= 4'b0000;
    end
end

//`#end` -- edit above this line, do not edit this line
endmodule
//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line

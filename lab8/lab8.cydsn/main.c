/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdlib.h>
#include <stdio.h>

#define USBFS_DEVICE (0u)
#define OUT_EP_NUM (1u)
#define IN_EP_NUM (2u)
#define BUFFER_SIZE 64
#define MIN_SPEED 50000
#define MAX_SPEED 154

uint16 length = 0;
uint8 obuffer[BUFFER_SIZE];
uint8 ibuffer[BUFFER_SIZE];
float rpm = 1000;
uint16 clkDivider = 5000;

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    Motor_CTL_Write(0x00);                              //motor is off
    CLK_SetDividerValue(clkDivider);
   
    USBFS_Start(USBFS_DEVICE, USBFS_5V_OPERATION);   
    while(0u == USBFS_GetConfiguration()){} //wait until we are enumerated by host
    USBFS_EnableOutEP(OUT_EP_NUM);
    
    for(;;)
    {
        /* Place your application code here. */
        
        //This double checks the input 
        if (0u != USBFS_IsConfigurationChanged())
        {
            /* Re-enable endpoint when device is configured. */
            if (0u != USBFS_GetConfiguration())
            {
                /* Enable OUT endpoint to receive data from host. */
                USBFS_EnableOutEP(OUT_EP_NUM);
            }
        }
        //This checks to see if there is any data from the host 
        if (USBFS_OUT_BUFFER_FULL == USBFS_GetEPState(OUT_EP_NUM))
        {
            /* Read number of received data bytes. */
            length = USBFS_GetEPCount(OUT_EP_NUM);

            /* Read data from OUT endpoint buffer. */
            USBFS_ReadOutEP(OUT_EP_NUM, obuffer, length);
            
            //Now, check to see if the data is a command            
            /*This checks to see if we want to speed up*/
            if (strcmp(obuffer,"UP") == 0) {
                if(clkDivider > MAX_SPEED) {
                    clkDivider = clkDivider - ((clkDivider > 2000) ? 2000 : 40);
                    if (clkDivider < MAX_SPEED) clkDivider = MAX_SPEED;
                    else if (clkDivider > MIN_SPEED) clkDivider = MIN_SPEED;
                }
                CLK_SetDividerValue(clkDivider);
                rpm = (100000.0/(float)clkDivider)*60.0/48.0; //48 is # step per rev
                sprintf((char *)ibuffer,"%.0f", rpm);
			 /* This sends the rpm to the host over USB */
                USBFS_LoadInEP(IN_EP_NUM,ibuffer,BUFFER_SIZE);
            }
            /*This checks to see if we want to speed down*/
            else if (strcmp(obuffer,"DOWN") == 0) {
                if(clkDivider < MIN_SPEED) {
                    clkDivider = clkDivider + ((clkDivider < 2000)? 40 : 2000);
                    if (clkDivider > MIN_SPEED) clkDivider = MIN_SPEED;
                    else if (clkDivider < MAX_SPEED) clkDivider = MAX_SPEED;
                }
                CLK_SetDividerValue(clkDivider);
                rpm = (100000.0/(float)clkDivider)*60.0/48.0;
                sprintf((char *)ibuffer,"%.0f", rpm);
			 /* This sends the rpm to the host over USB */
                USBFS_LoadInEP(IN_EP_NUM,ibuffer,BUFFER_SIZE);
            }
            /*This checks to see if we should turn it on*/
            else if (strcmp(obuffer, "ON") == 0) {                              //starts with the direction being cw & enable bit high
                Motor_CTL_Write(Motor_CTL_Read() | 0b00000011);
            }
		  /* This checks to see if we should turn it off */
            else if (strcmp(obuffer, "OFF") == 0) {
                Motor_CTL_Write(Motor_CTL_Read() & 0b11111110);                 //enable bit low
                //PUT CODE HERE TO TURN MOTOR OFF!
            }
		  /* This checks to see if we should turn it CCW */
            else if (strcmp(obuffer, "CW") == 0) {
                Motor_CTL_Write(Motor_CTL_Read() | 0b00000010);             //direction bit cw
                //PUT CODE HERE TO TURN MOTOR CW!
            }
		  /* This checks to see if we should turn it CCW */
            else if (strcmp(obuffer, "CCW") == 0) {                         //direction bit ccw
                Motor_CTL_Write(Motor_CTL_Read() & 0b11111101);
                //PUT CODE HERE TO TURN MOTOR CCW!
            }
		  /* Clear the receive buffer */
            memset(obuffer,0,BUFFER_SIZE);
        }           
    }
}
/* [] END OF FILE */

